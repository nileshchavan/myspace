package com.innominds.myspace.employeeTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import com.innominds.myspace.dao.AppraisalApprovedDao;
import com.innominds.myspace.dao.AppraisalRequestDao;
import com.innominds.myspace.dao.DriveDao;
import com.innominds.myspace.dao.EmployeeDao;
import com.innominds.myspace.dao.LoginCredentialsDao;
import com.innominds.myspace.dao.RaveDao;
import com.innominds.myspace.dao.SearchEmployeeDao;
import com.innominds.myspace.dao.SuccessfullProjectsDao;
import com.innominds.myspace.entities.AppraisalApproved;
import com.innominds.myspace.entities.AppraisalRequest;
import com.innominds.myspace.entities.Drives;
import com.innominds.myspace.entities.EmployeeDetails;
import com.innominds.myspace.entities.LoginCredentials;
import com.innominds.myspace.entities.Rave;
import com.innominds.myspace.entities.SuccessfullProjects;
import com.innominds.myspace.service.EmployeeServiceImpl;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class EmployeeServiceTest {

	@InjectMocks
	EmployeeServiceImpl employeeService = new EmployeeServiceImpl();

	@Mock
	EmployeeDao empDao;

	@Mock
	DriveDao driveDao;

	@Mock
	SuccessfullProjectsDao successDao;

	@Mock
	LoginCredentialsDao loginDao;

	@Mock
	AppraisalRequestDao appraisalDao;

	@Mock
	private SearchEmployeeDao searchEmployeeDao;

	@Mock
	AppraisalApprovedDao approvedDao;

	@Mock
	RaveDao raveDao;

	/**
	 * This method test get Employee by emp id method of service class
	 */
	@Test
	public void findByIdTest() {
		EmployeeDetails details = new EmployeeDetails();
		details.setEmployeeId(10343);
		details.setDepartment("UX");
		details.setName("Nilesh");

		when(empDao.findById(10343)).thenReturn(Optional.of(details));

		assertEquals("Nilesh", employeeService.findById(10343).getName());
	}

	@Test
	public void getAllDrivesTest() {
		List<Drives> list = new ArrayList<Drives>();
		Drives drive = new Drives();
		drive.setTitle("Web Developer");
		drive.setCreatedAt(new Date());
		drive.setCreatedBy(10343);
		drive.setDiscription("We are Hiring web developer");
		list.add(drive);
		when(driveDao.getAllDrives(new Date())).thenReturn(list);
		assertEquals(1, employeeService.getAllDrives().size());
//		verify(driveDao,times(1)).getAllDrives(new Date());
	}

	@Test
	public void getAllSuccessfullProjectsTest() {
		List<SuccessfullProjects> list = new ArrayList<SuccessfullProjects>();
		SuccessfullProjects projects = new SuccessfullProjects();
		projects.setProject_name("Yo Aspire");
		projects.setDescription("This is Yo aspire project");
		projects.setProject_head_id(10343);
		list.add(projects);
		when(successDao.findAll()).thenReturn(list);
		// verify(successDao,times(0)).findAll();
		assertEquals(1, employeeService.getAllSuccessfullProjects().size());

	}

	@Test
	public void doLoginTest() {

		LoginCredentials login = new LoginCredentials();
		login.setPassword("TestData@123");
		login.setUsername("Testdata");
		when(loginDao.save(login)).thenReturn(login);
		assertNotNull(loginDao.save(login));
		verify(loginDao, times(1)).save(login);
	}

	@Test
	public void appraisalRequestTest() {
		AppraisalRequest appraisal = new AppraisalRequest();
		appraisal.setAppraisalId(1);
		appraisal.setEmployeeId(10143);
		when(appraisalDao.save(appraisal)).thenReturn(appraisal);
		assertNotNull(employeeService.request(appraisal));
		verify(appraisalDao, times(1)).save(appraisal);
	}

	@Test
	public void getAppraisalApprovedTest() {
		AppraisalApproved approved = new AppraisalApproved();
		approved.setAppraisalId(10343);
		approved.setApprovalDate(new Date());
		approved.setPerformancePoints(8);
		when(approvedDao.findByemployeeId(10343)).thenReturn(approved);
		assertEquals(8, employeeService.getApproved(10343).getPerformancePoints());
	}

	@Test
	public void getLastIndexDriveTest() {
		Drives drive = new Drives();
		drive.setTitle("Web Developer");
		drive.setCreatedAt(new Date());
		drive.setCreatedBy(10343);
		drive.setDiscription("We are Hiring web developer");
		long index = 2;
		when(driveDao.count()).thenReturn(index);
		when(driveDao.findById(2)).thenReturn(Optional.of(drive));
		assertEquals(10343, employeeService.getLastIndexDrive().getCreatedBy());
		assertNotNull(employeeService.getLastIndexDrive());

	}

	@Test
	public void getLastIndexRaveTest() {
		Rave rave = new Rave();
		rave.setCreatedAt(new Date());
		rave.setRaveCompliment("very good");
		long index = 2;
		when(raveDao.count()).thenReturn(index);
		when(raveDao.findById(2)).thenReturn(Optional.of(rave));
		assertEquals("very good", employeeService.getLastIndexRave().getRaveCompliment());

	}

	@Test
	public void updateProfileTest() {
		EmployeeDetails details = new EmployeeDetails();
		details.setEmployeeId(10343);
		details.setDepartment("UX");
		details.setName("Nilesh");

		when(empDao.findById(10343)).thenReturn(Optional.of(details));
		when(empDao.save(details)).thenReturn(details);

		assertEquals("Nilesh", employeeService.updateProfile(details).getName());
	}

	/**
	 * test case for search the employee beased on the employee name
	 */
	@Test
	public void searchByName() {
		EmployeeDetails employeeDetails = new EmployeeDetails();
		employeeDetails.setName("lavanya");
		employeeDetails.setEmployeeId(10371);
		employeeDetails.setDesignation("Developer");

		when(searchEmployeeDao.findByname("lavanya")).thenReturn(employeeDetails);

		assertEquals("lavanya", employeeService.searchByName(employeeDetails.getName()).getName());

		// ---->Fails
		// assertEquals("lavaya",
		// employeeServiceImpl.searchByName(employeeDetails.getName()).getName());

	}

	@Test
	public void getLatestNotification() {
		List<Drives> driveList = new ArrayList<Drives>();

		List<Rave> raveList = new ArrayList<Rave>();

		java.util.Date date = new java.util.Date();
		Drives drive = new Drives();
		drive.setCreatedBy(10371);
		drive.setCreatedAt(date);
		drive.setDiscription("Testing");
		drive.setDriveDate(new Date("2019/3/11"));

		driveList.add(drive);

		Rave rave = new Rave();
		rave.setCreatedBy(10371);
		rave.setCreatedAt(new java.util.Date());
		rave.setRaveCompliment("Good Job");
		rave.setEmployeeId(10352);
		raveList.add(rave);
		Optional<Rave> emp = raveDao.findById(1);
		// when(raveDao.findById(raveList.size()).thenReturn(rave);
		when(emp).thenReturn(emp);

		Optional<Drives> drives = driveDao.findById(1);
		when(drives).thenReturn(drives);
		// when(employeeServiceImpl.getLastIndexRave()).thenReturn(rave);
		assertTrue((Rave) employeeService.getLatestNotification() instanceof Rave);

		assertTrue((Drives) employeeService.getLatestNotification() instanceof Drives);

	}
}

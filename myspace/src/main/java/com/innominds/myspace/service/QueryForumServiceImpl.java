package com.innominds.myspace.service;

import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.innominds.myspace.dao.AnswerDao;
import com.innominds.myspace.dao.QuestionDao;
import com.innominds.myspace.entities.Answer;
import com.innominds.myspace.entities.Question;

/**
 * this is implementation class for QueryForumService interface
 * 
 * @author myspcae-A
 *
 */
@Service
@Transactional
public class QueryForumServiceImpl implements QueryForumService {
	@Autowired
	private QuestionDao questionDao;
	@Autowired
	private AnswerDao answerDao;

	/**
	 * this method is for delete the question based on questionId
	 * 
	 * @param questionId
	 * 
	 * @return 0 if deleted, else -1
	 */
	@Override
	public int deleteQuestion(int questionId) {
		if (questionDao.existsById(questionId)) {
			questionDao.deleteById(questionId);
			return 0;
		} else {
			return -1;
		}
	} // End of deleteQuestion

	/**
	 * this method is for to post the question
	 *
	 * @param question
	 * @param postedBy
	 * 
	 * @return -1 if fails, else 0
	 */
	@Override
	public int postQuestion(Question question) {
		question.setPostedAt(Calendar.getInstance().getTime());
		Question quest = questionDao.save(question);

		if (quest != null) {
			return 0;
		} else {
			return -1;
		}

	}

	/**
	 * This method contains service logic to post(save) an answer for a question.
	 * 
	 * @param postedBy
	 * @param questionId
	 * @return 0 for posted successfully, else -1
	 */
	@Override
	public int postAnswer(Answer answer) {
		// if question is existed then answer will be saved and returns 0
		if (questionDao.existsById(answer.getQuestionId())) {
			answer.setPostedAt(Calendar.getInstance().getTime());
			answerDao.save(answer);
			return 0;
		}
		// if saving answer is failed -1 will return
		return -1;
	} // End of postAnswer

	/**
	 * This method used to fetch all answers of a question.
	 * 
	 * @param questionId
	 * 
	 * @return list of answers
	 */
	@Override
	public List<Answer> fetchAllAnswersOfQuestion(int questionId) {

		return answerDao.findAllByQuestionId(questionId);

	}

	/**
	 * service method for delete answer based on the answerID first it will search
	 * the answer existed or not
	 * 
	 * @param answerId
	 * @return if answerId exists it will delete the answer and return 0 else it
	 *         will return -1
	 */
	@Override
	public int deleteAnswer(int answerId) {

		if (answerDao.existsById(answerId)) {

			answerDao.deleteById(answerId);

			return 0;

		} else {

			return -1;

		}

	}

	/**
	 * service method for fetch all the questions
	 * 
	 * @return if the questions exist it will return QuestionList else it will
	 *         return null
	 */
	@Override
	public List<Question> fetachAllQuestion() {
		List<Question> question_list = null;
		question_list = (List<Question>) questionDao.findAll();
		return question_list;

	}

	/**
	 * this method is for to update the existed answer
	 * 
	 * @param answerId
	 * @param postedBy
	 * @param answer
	 * @param questionId
	 * 
	 * @return 0 for successfull updations, else -1
	 * 
	 */
	@Override
	public int updateAnswer(Answer answer) {

		if (answer != null) {
			String existingAnswer = answerDao.findById(answer.getAnswerId()).get().getAnswer();
			if (existingAnswer.equals(answer.getAnswer())) {
				return -2;
			}
			answer.setPostedAt(Calendar.getInstance().getTime());
			answerDao.save(answer);
			return 0;
		} else {
			return -1;
		}

	}

}

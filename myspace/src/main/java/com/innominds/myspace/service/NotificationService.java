package com.innominds.myspace.service;

import java.util.List;

import com.innominds.myspace.entities.Rave;

/**
 * This is a interface NotificationService which having the following methods
 * those implementation is provided in NotificationServiceImpl class
 * 
 * @author myspace-A
 *
 */
public interface NotificationService {

	List<Rave> getAllRave();

	List<Rave> getAllRaveByEmpId(int empId);

	int insertNewRave(Rave rave);

	List<Rave> getAllRavePostedBy(int empId);
}

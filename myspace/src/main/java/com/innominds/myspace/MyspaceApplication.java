package com.innominds.myspace;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.innominds.myspace.property.FileStorageProperties;

/**
 * This is the starting point of a application
 * @author IMVIZAG
 *
 */
@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
public class MyspaceApplication {

	public static void main(String[] args) {
		
		//bootstrapping application
		SpringApplication.run(MyspaceApplication.class, args);
		
	}
}


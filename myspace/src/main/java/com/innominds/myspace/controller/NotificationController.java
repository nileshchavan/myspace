package com.innominds.myspace.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innominds.myspace.entities.Rave;
import com.innominds.myspace.service.NotificationService;
import com.innominds.myspace.util.JwtImpl;
import com.innominds.myspace.util.Validations;

/**
 * This controller class contains web resources which handles request for
 * notifications that include drive and rave notifications.
 * 
 * @author IMVIZAG
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/myspace/notifications")
public class NotificationController {
	@Autowired
	private NotificationService notificationService;
	@Autowired
	private Validations validations;

	/**
	 * This web resource handles the request for getting all rave postings. For
	 * valid user it will process the request. If there are no rave it will return
	 * status like no rave available. else returns list of rave in response.
	 * 
	 * @param id      for token verification
	 * @param headers contains token and content-type
	 * @return ResponseEntity includes status, status code and result.
	 */
	@CrossOrigin
	@GetMapping("/rave/all/{employee_id}")
	public ResponseEntity<?> getAllRave(@PathVariable("employee_id") int id, @RequestHeader HttpHeaders headers) {
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		if (JwtImpl.isValidUser(id, token)) {
			// calling service method
			List<Rave> raveList = notificationService.getAllRave();
			Map<String, Object> responseList = new HashMap<>();
			ObjectMapper mapper = new ObjectMapper();
			String jsonResp = null;
			// if no rave notifications available below code will execute
			if (raveList.isEmpty()) {
				responseList.put("status", "No Rave available");
				responseList.put("statuscode", 204);
				try {
					jsonResp = mapper.writeValueAsString(responseList);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
			}
			// if result contains rave notifications then below code will execute and
			// returns response
			responseList.put("statuscode", 200);
			responseList.put("result", raveList);
			try {
				jsonResp = mapper.writeValueAsString(responseList);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
			return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of getAllRave

	/**
	 * This web resource handles the request for all personal rave posted by
	 * employee who logged in. For valid user it will process the request. If there
	 * are no rave it will return status like no rave available. else returns list
	 * of rave in response.
	 * 
	 * @param id      for token verification
	 * @param headers contains token and content-type
	 * @return ResponseEntity includes status, status code and result.
	 */
	@CrossOrigin
	@GetMapping("/rave/allpersonalraves/{employeeId}")
	public ResponseEntity<?> getAllRaveById(@PathVariable("employeeId") int id, @RequestHeader HttpHeaders headers) {
		String token = "";

		Map<String, Object> responseList = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();
		String jsonResp = null;

		List<Rave> raveList = null;

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid token\",\"statuscode\":" + 202 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);
		if (JwtImpl.isValidUser(id, token)) {
			// calling service method
			String empId = "" + id;
			System.out.println(empId);
			if (validations.employeeIdValidator(empId)) {
				raveList = notificationService.getAllRaveByEmpId(id);

				if (raveList.isEmpty()) {
					responseList.put("status", "No Rave available");
					responseList.put("statuscode", 204);
					try {
						jsonResp = mapper.writeValueAsString(responseList);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
				}
				// if rave notifications available below code will execute
				responseList.put("statuscode", 200);
				responseList.put("raves got", raveList);
				try {
					jsonResp = mapper.writeValueAsString(responseList);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
			}

			else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Invalid EmployeeId\",\"statuscode\":" + 202 + "}");
			}

			// if no rave notifications then below code will execute

		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of getAllRaveById

	/**
	 * This web resource handles the request for posting new rave. For valid user it
	 * will process the request. Any employee who logged in can post rave.
	 * 
	 * @param rave    contains rave information
	 * @param headers contains token and content-type
	 * @return ResponseEntity includes status, status code and result.
	 */
	@CrossOrigin
	@PostMapping("/rave/postnew")
	public ResponseEntity<?> postNewRave(@RequestBody Rave rave, @RequestHeader HttpHeaders headers) {

		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);
		int id = rave.getCreatedBy();
		if (JwtImpl.isValidUser(id, token)) {
			// calling service method
			int status = notificationService.insertNewRave(rave);
			// if rave posted successfully below code execute
			if (status == 0) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"rave posted successfully\",\"statuscode\":" + 200 + "}");
			} else if (status == -1) {
				// if any failure below code will execute and returns status code 201 in json
				// format.
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"posting rave failed\",\"statuscode\":" + 201 + "}");
			} else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"employeeId doesn't exist\",\"statuscode\":" + 202 + "}");
			}
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of getAllRaveById

	/**
	 * This web resource handles the request for all posted rave notification by an
	 * employee. For valid user it will process the request.
	 * 
	 * @param id      for token verification
	 * @param headers token and content-type
	 * @return ResponseEntity includes status, status code and result.
	 */
	@CrossOrigin
	@GetMapping("/rave/allpostedraves/{employeeId}")
	public ResponseEntity<?> getAllRavePostedBy(@PathVariable("employeeId") int id,
			@RequestHeader HttpHeaders headers) {
		String token = "";
		List<Rave> raveList = null;
		Map<String, Object> responseList = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();
		String jsonResp = null;

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);
		if (JwtImpl.isValidUser(id, token)) {
			// calling service method

			String empId = "" + id;
			if (validations.employeeIdValidator(empId)) {
				raveList = notificationService.getAllRavePostedBy(id);

				if (raveList.isEmpty()) {
					responseList.put("status", "No Rave postings");
					responseList.put("statuscode", 204);
					try {
						jsonResp = mapper.writeValueAsString(responseList);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}
					return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
				}
				// if rave notifications available below code will execute
				responseList.put("statuscode", 200);
				responseList.put("rave postings", raveList);
				try {
					jsonResp = mapper.writeValueAsString(responseList);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
			} else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Invalid empId\",\"statuscode\":" + 202 + "}");
			}

			// if no rave notifications then below code will execute

		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of getAllRavePostedBy

}
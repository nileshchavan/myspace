package com.innominds.myspace.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innominds.myspace.entities.Answer;
import com.innominds.myspace.entities.Question;
import com.innominds.myspace.service.QueryForumService;
import com.innominds.myspace.util.JwtImpl;

/**
 * This controller class contains web resource end points which are responsible
 * to handle the requests from client regarding query forum.
 * 
 * @author myspace Team-A
 *
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/myspace/queryforum")
public class QueryForumControllerImpl {

	@Autowired
	private QueryForumService queryService;

	/**
	 * This web resource handles the request to delete a question. For valid user it
	 * will process the request.
	 * 
	 * @param empId      for token verification
	 * @param questionId based on this id deletion will performed
	 * @param headers    contains token and content-type information
	 * @return ResponseEntity
	 */
	@CrossOrigin
	@DeleteMapping("/deletequestion/{empId}/{questionId}")
	public ResponseEntity<?> deleteQuestion(@PathVariable("empId") int empId,
			@PathVariable("questionId") int questionId, @RequestHeader HttpHeaders headers) {
		int isdeleted = 1;
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		// return true if token is a valid and allowing to do further task
		if (JwtImpl.isValidUser(empId, token)) {
			// performing deletion operation over service layer

			isdeleted = queryService.deleteQuestion(questionId);
			if (isdeleted == -1) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"deletion failed\",\"statuscode\":" + 202 + "}");
			} else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"deleted successfully\",\"statuscode\":" + 200 + "}");
			}
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of deleteQuestion

	/**
	 * This web resource handles the request from client to post new answer for a
	 * question. For valid user it will process the request.
	 * 
	 * @param answer  contains answer information
	 * @param headers contains token and content-type information
	 * @return ResponseEntity
	 */
	@CrossOrigin
	@PostMapping("/postnewanswer")
	public ResponseEntity<?> postNewAnswer(@RequestBody Answer answer, @RequestHeader HttpHeaders headers) {
		int isposted = 1;
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		// return true if token is a valid and allowing to do further task
		if (JwtImpl.isValidUser(answer.getPostedBy(), token)) {
			// performing deletion operation over service layer

			isposted = queryService.postAnswer(answer);

			if (isposted == -1) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"posting answer failed\",\"statuscode\":" + 202 + "}");
			} else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"posted successfully\",\"statuscode\":" + 200 + "}");
			}
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of postNewAnswer

	/**
	 * This web resource handles the request to get all the answers for a
	 * question.For valid user it will process the request. If no answers for that
	 * question it will return status like no answers available.
	 * 
	 * @param empId      for token verification
	 * @param questionId to recognize uniquely
	 * @param headers    contains token and content-type information
	 * @return ResponseEntity
	 */
	@CrossOrigin
	@GetMapping("/getallanswersforquestion/{empId}/{questionId}")
	public ResponseEntity<?> getAllAnswersForQuestion(@PathVariable("empId") int empId,
			@PathVariable("questionId") int questionId, @RequestHeader HttpHeaders headers) {
		List<Answer> answerList = null;
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		// return true if token is a valid and allowing to do further task
		if (JwtImpl.isValidUser(empId, token)) {
			// performing deletion operation over service layer

			answerList = queryService.fetchAllAnswersOfQuestion(questionId);
			Map<String, Object> responseList = new HashMap<>();
			ObjectMapper mapper = new ObjectMapper();
			String jsonResp = null;

			if (answerList == null || answerList.isEmpty()) {
				responseList.put("status", "No answers found");
				responseList.put("statuscode", 204);
				try {
					jsonResp = mapper.writeValueAsString(responseList);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
			} else {
				responseList.put("statuscode", 200);
				responseList.put("result", answerList);
				try {
					jsonResp = mapper.writeValueAsString(responseList);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
			}
		}
		return ResponseEntity.status(HttpStatus.OK).body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
	} // End of getAllAnswersForQuestion

	/**
	 * this is the controller method to post the question if question posted
	 * successfully then it will return success status code else it will return fail
	 * status code
	 */
	/**
	 * This web resource handles the request to post a question. For valid user it
	 * will process the request. It will return status like success or failure.
	 * 
	 * @param empId    for token verification
	 * @param question contains question information
	 * @param headers  contains token and content-type information
	 * @return ResponseEntity
	 */
	@CrossOrigin
	@PostMapping("/postquestion/{empId}")
	public ResponseEntity<?> postQuestion(@PathVariable("empId") int empId, @RequestBody Question question,
			@RequestHeader HttpHeaders headers) {

		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Header desn't contain token returning invalid user
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// Assigning authentication token to variable
		token = hList.get(0);

		if (JwtImpl.isValidUser(empId, token)) {
			int statusOfPostQuestion = queryService.postQuestion(question);
			System.out.println(statusOfPostQuestion);

			if (statusOfPostQuestion == 0) {

				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"Question posted\",\"statuscode\":" + 200 + "}");
			}

			else {

				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"failed to post the question\",\"statuscode\":" + 202 + "}");
			}

		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}// postQuestion-->end

	/**
	 * This web resource handles the request for delete an answer for a question.
	 * For valid user it will process the request. In deleting answer question will
	 * not effect. It will return status as success or failed.
	 * 
	 * @param empId    for token verification.
	 * @param answerId to identify answer uniquely
	 * @param headers  contains token and content-type information
	 * @return ResponseEntity
	 */
	@CrossOrigin
	@DeleteMapping("/deleteAnswer/{empId}/{answerId}")
	public ResponseEntity<?> deleteAnswer(@PathVariable("empId") int empId, @PathVariable("answerId") int answerId,
			@RequestHeader HttpHeaders headers) {

		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Header desn't contain token returning invalid user
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// Assigning authentication token to variable
		token = hList.get(0);

		if (JwtImpl.isValidUser(empId, token)) {

			int delete_answer_status = queryService.deleteAnswer(answerId);

			if (delete_answer_status == 0) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"deleted successfully\",\"statuscode\":" + 200 + "}");

			} else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"the answer ID doesn't exist\",\"statuscode\":" + 202 + "}");

			}

		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	}// end

	/**
	 * This web resource handles the request for getting all questions. For valid
	 * user it will process the request. It will return all questions as list by
	 * including in response entity. If there are no questions it will give status
	 * as no questions found.
	 * 
	 * @param empId   for token verification
	 * @param headers contains token and content-type information
	 * @return ResponseEntity
	 */
	@CrossOrigin
	@GetMapping("/getallquestions/{empId}")
	public ResponseEntity<?> getAllQuestions(@PathVariable("empId") int empId, @RequestHeader HttpHeaders headers) {

		String token = "";

		Map<String, Object> responseList = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();
		String jsonResp = null;

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Header desn't contain token returning invalid user
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// Assigning authentication token to variable
		token = hList.get(0);

		if (JwtImpl.isValidUser(empId, token)) {

			List<Question> questions_list = queryService.fetachAllQuestion();

			if (questions_list.isEmpty()) {
				responseList.put("status", "No questions found");
				responseList.put("statuscode", 202);
				try {
					jsonResp = mapper.writeValueAsString(responseList);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
			} else {
				responseList.put("statuscode", 200);
				responseList.put("Questions_list", questions_list);
				try {
					jsonResp = mapper.writeValueAsString(responseList);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				return ResponseEntity.status(HttpStatus.OK).body(jsonResp);
			}

		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
	}

	/**
	 * This web resource handles the request form client to update answer for a
	 * question. For valid user it will process the request. Answer can update if
	 * this answer should posted by same employee.
	 * 
	 * @param answer  contains answer information
	 * @param headers contains token and content-type information
	 * @return ResponseEntity
	 */
	@CrossOrigin
	@PutMapping("/updateanswer")
	public ResponseEntity<?> updateAnswer(@RequestBody Answer answer, @RequestHeader HttpHeaders headers) {
		int isupdated = 1;
		String token = "";

		// getting Header List from HttpHeader
		List<String> hList = headers.get("Authorization");

		// if Request not contain Header returning Invalid User
		if (hList == null) {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}
		// getting token from Herder List
		token = hList.get(0);

		// return true if token is a valid and allowing to do further task
		if (JwtImpl.isValidUser(answer.getPostedBy(), token)) {
			// performing deletion operation over service layer

			isupdated = queryService.updateAnswer(answer);
			if (isupdated == -1) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"deletion failed\",\"statuscode\":" + 202 + "}");
			} else if (isupdated == -2) {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"no modification in answer\",\"statuscode\":" + 203 + "}");
			} else {
				return ResponseEntity.status(HttpStatus.OK)
						.body("{\"status\":\"updated successfully\",\"statuscode\":" + 200 + "}");
			}
		} else {
			return ResponseEntity.status(HttpStatus.OK)
					.body("{\"status\":\"Invalid User\",\"statuscode\":" + 201 + "}");
		}

	} // End of updateAnswer

}

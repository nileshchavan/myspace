package com.innominds.myspace.util;

public enum TokenTimeOut {
	    ONE_HOUR(60 * 60 * 1000), HALF_MINUTE(30 * 1000);
	    public final long value;

	    TokenTimeOut(int value) {
	        this.value = value;
	    }
	
}

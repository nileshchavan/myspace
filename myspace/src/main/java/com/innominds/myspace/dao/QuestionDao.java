package com.innominds.myspace.dao;

import org.springframework.data.repository.CrudRepository;

import com.innominds.myspace.entities.Question;

public interface QuestionDao extends CrudRepository<Question, Integer> {

}

package com.innominds.myspace.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.innominds.myspace.entities.AppraisalApproved;

/**
 * This interface provides find Method AppraisalApproved
 * @author IMVIZAG
 *
 */
public interface AppraisalApprovedDao extends JpaRepository<AppraisalApproved, Integer>{
	
	//preparing query for getting appraisal request status
	//@Query(value = "SELECT * FROM appraisal_approved WHERE employee_id = ?1", nativeQuery = true)
	public AppraisalApproved findByemployeeId(int employeeId);
}

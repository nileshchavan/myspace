package com.innominds.myspace.entities;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This entity class represents respective table in database and annotated with
 * jpa annotations.
 * 
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "login_credentials")
public class LoginCredentials {
	@Id
	private int id;
	private String username;
	private String password;
	@Column(name = "employee_id")
	private int employeeId;
	@Column(name = "active_status")
	private String status;
	@Column(name = "attempts")
	private int attempts;
	@Column(name = "first_wrong_attempt_datetime")
	private Timestamp firstWrongAttemptdatetime;
	@Column(name = "blocked_datetime")
	private Timestamp blockedDateTime;
	@Column(name = "login_time")
	private Timestamp loginTime;

	public LoginCredentials() {
		super();
	}

	public LoginCredentials(int id, String username, String password, int employeeId, String status, int attempts,
			Timestamp firstWrongAttemptdatetime, Timestamp blockedDateTime,Timestamp loginTime) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.employeeId = employeeId;
		this.status = status;
		this.attempts = attempts;
		this.firstWrongAttemptdatetime = firstWrongAttemptdatetime;
		this.blockedDateTime = blockedDateTime;
		this.loginTime = loginTime;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getAttempts() {
		return attempts;
	}

	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}

	public Timestamp getBlockedDateTime() {
		return blockedDateTime;
	}

	public void setBlockedDateTime(Timestamp blockedDateTime) {
		this.blockedDateTime = blockedDateTime;
	}

	public Timestamp getFirstWrongAttemptdatetime() {
		return firstWrongAttemptdatetime;
	}

	public void setFirstWrongAttemptdatetime(Timestamp firstWrongAttemptdatetime) {
		this.firstWrongAttemptdatetime = firstWrongAttemptdatetime;
	}

	public Timestamp getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Timestamp loginTime) {
		this.loginTime = loginTime;
	}

	
}

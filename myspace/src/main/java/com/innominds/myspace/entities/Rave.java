package com.innominds.myspace.entities;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * This Entity class gets and post RAVE for employee
 * @author IMVIZAG
 *
 */

@Entity
@Table(name = "rave")
public class Rave {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "rave_id")
	private int raveId;

	@Column(name = "employee_id")
	private int employeeId;
	@Column(name = "discription")
	private String discription;
	@Column(name = "created_by")
	private int createdBy;

	@Column(name = "created_at")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone="GMT+5:30")
	private Date createdAt;
	@Column(name = "rave_compliment")
	private String raveCompliment;

	public Rave() {
		super();
	}

	public Rave(int raveId, int employeeId, String discription, int createdBy, Date createdAt, String raveCompliment) {
		super();
		this.raveId = raveId;
		this.employeeId = employeeId;
		this.discription = discription;
		this.createdBy = createdBy;
		this.createdAt = createdAt;
		this.raveCompliment = raveCompliment;
	}



	public int getRaveId() {
		return raveId;
	}

	public void setRaveId(int raveId) {
		this.raveId = raveId;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getRaveCompliment() {
		return raveCompliment;
	}

	public void setRaveCompliment(String raveCompliment) {
		this.raveCompliment = raveCompliment;
	}

	

}

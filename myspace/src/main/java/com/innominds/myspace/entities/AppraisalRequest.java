package com.innominds.myspace.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * This Entity class for requesting appraisal approval
 * @author IMVIZAG
 *
 */
@Entity
@Table(name="appraisal_request")
public class AppraisalRequest {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="appraisal_id")
	private int appraisalId;
	
	@Column(name="employee_id")
	private int employeeId;
	
	@Column(name="requested_date")
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date requestedDate;
	
	
	//getters and Settters for rquest appraisal approval
	public int getAppraisalId() {
		return appraisalId;
	}
	public void setAppraisalId(int appraisalId) {
		this.appraisalId = appraisalId;
	}
	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public Date getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	
	
}

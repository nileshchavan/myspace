package com.innominds.myspace.entities;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
/**
 * This Entity class Gets and sets New Employee in database
 * @author IMVIZAG
 *
 */
@Entity
@Table(name = "employee_details")
public class EmployeeDetails {

	@Id
	@Column(name = "employee_id")
	private int employeeId;

	@Column(name = "sez_id")
	private String sezId;

	@Column(name = "name")
	private String name;

	@Column(name = "sez_joining_date")
	
	@Basic
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date sezJoiningDate;

	@Column(name = "employee_type")
	private String employmentType;

	@Column(name = "department")
	private String department;

	@Column(name = "designation")
	private String designation;

	@Column(name = "practice")
	private String practice;

	@Column(name = "competency")
	private String competency;

	@Column(name = "work_location")
	private String workLocation;

	@Column(name = "permanent_location")
	private String permanentLocation;

	@Column(name = "company_email_id")
	private String companyEmailId;

	@Column(name = "personal_email_id")
	private String personalEmailId;

	@Column(name = "business_email_id")
	private String businessEmailId;

	@Column(name = "work_place_phone_number")
	private String workPlacePhoneNumber;

	@Column(name = "primary_mobile_number")
	private String primaryMobileNumber;

	@Column(name = "alternative_mobile_number")
	private String alternativeMobileNumber;

	@Column(name = "skpe_id")
	private String skypeId;

	@Column(name = "job_status")
	private String jobStatus;

	@Column(name = "billable")
	private String billable;
	
	@Column(name="profile_pic")
	private String fileName;
	

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getSezId() {
		return sezId;
	}

	public void setSezId(String sezId) {
		this.sezId = sezId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getSezJoiningDate() {
		return sezJoiningDate;
	}

	public void setSezJoiningDate(Date sezJoiningDate) {
		this.sezJoiningDate = sezJoiningDate;
	}

	public String getEmploymentType() {
		return employmentType;
	}

	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getPractice() {
		return practice;
	}

	public void setPractice(String practice) {
		this.practice = practice;
	}

	public String getCompetency() {
		return competency;
	}

	public void setCompetency(String competency) {
		this.competency = competency;
	}

	public String getWorkLocation() {
		return workLocation;
	}

	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}

	public String getPermanentLocation() {
		return permanentLocation;
	}

	public void setPermanentLocation(String permanentLocation) {
		this.permanentLocation = permanentLocation;
	}

	public String getCompanyEmailId() {
		return companyEmailId;
	}

	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}

	public String getPersonalEmailId() {
		return personalEmailId;
	}

	public void setPersonalEmailId(String personalEmailId) {
		this.personalEmailId = personalEmailId;
	}

	public String getBusinessEmailId() {
		return businessEmailId;
	}

	public void setBusinessEmailId(String businessEmailId) {
		this.businessEmailId = businessEmailId;
	}

	public String getWorkPlacePhoneNumber() {
		return workPlacePhoneNumber;
	}

	public void setWorkPlacePhoneNumber(String workPlacePhoneNumber) {
		this.workPlacePhoneNumber = workPlacePhoneNumber;
	}

	public String getPrimaryMobileNumber() {
		return primaryMobileNumber;
	}

	public void setPrimaryMobileNumber(String primaryMobileNumber) {
		this.primaryMobileNumber = primaryMobileNumber;
	}

	public String getAlternativeMobileNumber() {
		return alternativeMobileNumber;
	}

	public void setAlternativeMobileNumber(String alternativeMobileNumber) {
		this.alternativeMobileNumber = alternativeMobileNumber;
	}

	public String getSkypeId() {
		return skypeId;
	}

	public void setSkypeId(String skypeId) {
		this.skypeId = skypeId;
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}

	public String getBillable() {
		return billable;
	}

	public void setBillable(String billable) {
		this.billable = billable;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	
	
	
	
}

package com.innominds.myspace.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * This is entity class which represents answer objects and binded with database
 * table.
 * 
 * @author venkatesh
 *
 */
@Entity
@Table(name = "answer_tbl")
public class Answer {

	// entity fields
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int answerId;
	/**
	 postedBy nothing but employeeId who posted this answer.
	 */
	@Column(name = "posted_by")
	private int postedBy;
	
	private String answer;
	
	@Column(name = "posted_at")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd",timezone="GMT+5:30")
	private Date postedAt;
	
	@Column(name = "question_id")
	private int questionId;

	// default constructor
	public Answer() {
		super();
	}

	// parameterized constructor
	public Answer(int answerId, int postedBy, String answer, Date postedAt, int questionId) {
		super();
		this.answerId = answerId;
		this.postedBy = postedBy;
		this.answer = answer;
		this.postedAt = postedAt;
		this.questionId = questionId;
	}

	// setters and getter methods

	public int getPostedBy() {
		return postedBy;
	}

	public int getAnswerId() {
		return answerId;
	}

	public void setAnswerId(int answerId) {
		this.answerId = answerId;
	}

	public void setPostedBy(int postedBy) {
		this.postedBy = postedBy;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Date getPostedAt() {
		return postedAt;
	}

	public void setPostedAt(Date postedAt) {
		this.postedAt = postedAt;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

}

package com.innominds.myspace.entities;


/**
 * This is simply bean class for searching another employee 
 * @author IMVIZAG
 *
 */
public class FindEmployee {

	private int employee_id;
	private String name;
	private String employmentType;
	private String designation;
	private String department;
	private String practice;
	private String workLocation;
	private String companyEmailId;
	private String permanentLocation;
	private String WorkStationId;
	public String getWorkStationId() {
		return WorkStationId;
	}
	public void setWorkStationId(String workStationId) {
		WorkStationId = workStationId;
	}
	public int getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmploymentType() {
		return employmentType;
	}
	public void setEmploymentType(String employmentType) {
		this.employmentType = employmentType;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getPractice() {
		return practice;
	}
	public void setPractice(String practice) {
		this.practice = practice;
	}
	public String getWorkLocation() {
		return workLocation;
	}
	public void setWorkLocation(String workLocation) {
		this.workLocation = workLocation;
	}
	public String getCompanyEmailId() {
		return companyEmailId;
	}
	public void setCompanyEmailId(String companyEmailId) {
		this.companyEmailId = companyEmailId;
	}
	public String getPermanentLocation() {
		return permanentLocation;
	}
	public void setPermanentLocation(String permanentLocation) {
		this.permanentLocation = permanentLocation;
	}
	
	
}
